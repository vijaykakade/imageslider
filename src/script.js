$(document).ready(function () {
    $.ajax({
        url: "https://jsonplaceholder.typicode.com/photos",
        cache: true,
        success: function (data) {
            let pageCount = Math.floor(data.length / 15);
            for (let i = 0; i <= data.length-10; i += 15) {
                $('<div class="item"><div class="imageslidewrapper"><img src="' + data[i].thumbnailUrl + '"><img src="' + data[i + 1].thumbnailUrl + '"><img src="' + data[i + 2].thumbnailUrl + '"><img src="' + data[i + 3].thumbnailUrl + '"><img src="' + data[i + 4].thumbnailUrl + '"><img src="' + data[i + 5].thumbnailUrl + '"><img src="' + data[i + 6].thumbnailUrl + '"><img src="' + data[i + 7].thumbnailUrl + '"><img src="' + data[i + 8].thumbnailUrl + '"><img src="' + data[i + 9].thumbnailUrl + '"><img src="' + data[i + 10].thumbnailUrl + '"><img src="' + data[i + 11].thumbnailUrl + '"><img src="' + data[i + 12].thumbnailUrl + '"><img src="' + data[i + 13].thumbnailUrl + '"><img src="' + data[i + 14].thumbnailUrl + '"></div></div>').appendTo('.carousel-inner');
            }
            $('.item').first().addClass('active');
            $('#myCarousel').carousel({
                pause: true,
                interval: false,
                cycle: false,
            });
            $('#pagination-here').bootpag({
                total: pageCount,
                page: 1,
                maxVisible: 10,
                leaps: true,
                href: "#result-page-{{number}}",
            })
            
            $('#pagination-here').on("page", function (event, num) {
                $('#myCarousel').carousel(num);
            });
            $('ul[class="bootpag"]').addClass('pagination');

            let totalItems = $('.item').length;
            let currentIndex = $('div.active').index();

            $('.next').click(function(){
                let currentIndex = $('div.active').index();
                $('#myCarousel').carousel(currentIndex);
                if(currentIndex == 0) {
                    $('#pagination-here').bootpag({
                        page: currentIndex + 2,
                    });
                    $('ul[class="bootpag"]').addClass('pagination');
                } else {
                    $('#pagination-here').bootpag({
                        page: currentIndex + 1,
                    });
                    $('ul[class="bootpag"]').addClass('pagination');
                }
                
            });

            $('.previous').click(function(){
                let currentIndex = $('div.active').index();
                $('#myCarousel').carousel(currentIndex - 1);
                if(currentIndex > 1) {
                    $('#pagination-here').bootpag({
                        page: currentIndex - 1,
                    });
                    $('ul[class="bootpag"]').addClass('pagination');
                }
            });

            let getNum = 0;
            let nameKey = '';
            
            //search
            $("#mySearchBtn").click(function(e){
                e.preventDefault();
                nameKey = $('#myInput').val();
                search(nameKey, data);
            });
            
            function search(nameKey, data){
                let resultObject = [];
                for (let i=0; i < data.length; i++) {
                    if (data[i].title.includes(nameKey)) {
                       resultObject.push(data[i]);
                    } 
                }
                console.log(resultObject);
                console.log(resultObject.length);
                $('.carousel-inner').html('');
                for (let i = 0; i <= resultObject.length-15; i += 15) {
                    $('<div class="item"><div class="imageslidewrapper"><img src="' + resultObject[i].thumbnailUrl + '"><img src="' + resultObject[i + 1].thumbnailUrl + '"><img src="' + resultObject[i + 2].thumbnailUrl + '"><img src="' + resultObject[i + 3].thumbnailUrl + '"><img src="' + resultObject[i + 4].thumbnailUrl + '"><img src="' + resultObject[i + 5].thumbnailUrl + '"><img src="' + resultObject[i + 6].thumbnailUrl + '"><img src="' + resultObject[i + 7].thumbnailUrl + '"><img src="' + resultObject[i + 8].thumbnailUrl + '"><img src="' + resultObject[i + 9].thumbnailUrl + '"><img src="' + resultObject[i + 10].thumbnailUrl + '"><img src="' + resultObject[i + 11].thumbnailUrl + '"><img src="' + resultObject[i + 12].thumbnailUrl + '"><img src="' + resultObject[i + 13].thumbnailUrl + '"><img src="' + resultObject[i + 14].thumbnailUrl + '"></div></div>').appendTo('.carousel-inner');
                }
                $('.item').first().addClass('active');
            }
           
        }
    });

    
});



